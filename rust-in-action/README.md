# Rust in Action

- [Main](../README.md)

## References

- ["Rust in Action" at Manning](https://www.manning.com/books/rust-in-action)
- [Source code for the book Rust in Action](https://github.com/rust-in-action/code)

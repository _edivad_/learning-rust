pub fn demo() {
    #[derive(Debug)]
    struct MeaningOfLife {
        answer: u8,
    }

    fn ask(mol: MeaningOfLife) {
        println!("{:?}", mol);
    }

    impl Copy for MeaningOfLife {}
    impl Clone for MeaningOfLife {
        fn clone(&self) -> Self {
            MeaningOfLife {
                answer: self.answer,
            }
        }
    }

    let mol = MeaningOfLife { answer: 42 };

    ask(mol);
    ask(mol); // won't work until Copy is implemented, can also be derived
}

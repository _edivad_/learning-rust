pub fn demo() {
    struct Dummy {
        foo: u8,
    }

    #[derive(Debug)]
    struct AnotherDummy {
        foo: u8,
    }

    let dummy = Dummy { foo: 123 };
    drop(dummy); // called implicitly at the end of the owner

    impl Drop for AnotherDummy {
        fn drop(&mut self) {
            println!("dropping {:?}", self);
        }
    }

    let another_dummy = AnotherDummy { foo: 123 };
    drop(another_dummy);
}

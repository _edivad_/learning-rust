#[derive(Debug)]
pub struct MeaningOfLife {
    answer: u8,
}

impl MeaningOfLife {
    pub fn new(answer: u8) -> MeaningOfLife {
        MeaningOfLife { answer }
    }

    pub fn get(&self) -> u8 {
        self.answer
    }
}

pub fn demo() {
    fn use_value_copy(_val: i32) {}

    let a = 123;
    use_value_copy(a);

    println!("{}", a); // primitive types can be used again

    fn use_value_move(_val: Dummy) {}

    struct Dummy {
        foo: i32,
    }

    let demo = Dummy { foo: 123 };
    use_value_move(demo);

    // println!("{}", demo.foo); // won't compile when uncommented
}

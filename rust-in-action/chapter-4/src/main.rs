mod common;
mod copy_clone;
mod copy_vs_move;
mod drop;
mod ownership_move;
mod references;
mod wrap;

fn main() {
    copy_vs_move::demo();
    drop::demo();
    ownership_move::demo();
    references::demo();
    copy_clone::demo();
    wrap::demo();
}

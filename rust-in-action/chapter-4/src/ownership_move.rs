use crate::common::MeaningOfLife;

pub fn demo() {
    fn ask(dummy: MeaningOfLife) {
        println!("doing something with {:?}", dummy);
    }

    let mol = MeaningOfLife::new(42);

    //
    // passing to a function
    //
    ask(mol);
    // let cannot_assign_mol = mol; // won't compile when uncommented, value moved

    //
    // assignment, variable binding
    //
    let another_mol = MeaningOfLife::new(42);
    // ...
    let _new_mol = another_mol;
    // println!("{:?}", another_mol); // won't compile when uncommented, value moved

    //
    // can return ownership
    //
    fn ask_v2(dummy: MeaningOfLife) -> MeaningOfLife {
        println!("doing something with {:?} and returning it", dummy);
        dummy
    }
    let yet_another_mol = MeaningOfLife::new(42);
    let yet_another_mol = ask_v2(yet_another_mol);
    println!("returned {:?}", yet_another_mol);
}

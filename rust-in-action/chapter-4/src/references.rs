use std::fmt::Debug;

pub fn demo() {
    #[derive(Debug)]
    struct Holder<T> {
        value: T,
    }

    fn show<T: Debug>(h: &Holder<T>) -> () {
        // note the use of reference
        println!("value: {:?}", h.value);
    }

    fn update<T>(h: &mut Holder<T>, value: T) -> () {
        h.value = value;
    }

    let mut h = Holder { value: 123 };
    show(&h);
    show(&h);

    update(&mut h, 456);
    show(&h);
}

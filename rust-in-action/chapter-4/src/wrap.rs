use std::rc::Rc;

pub fn demo() {
    #[derive(Debug)]
    struct MeaningOfLife {
        answer: u8,
    }

    impl Drop for MeaningOfLife {
        fn drop(&mut self) {
            println!("dropping {:?}", self);
        }
    }

    fn ask(mol: Rc<MeaningOfLife>) {
        println!("answer: {}", mol.answer);
    }

    let mol = Rc::new(MeaningOfLife { answer: 42 });

    ask(mol.clone());
    ask(mol.clone());
    ask(mol);
}

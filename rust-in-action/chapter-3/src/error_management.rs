pub fn demo() {
    let ok: Result<String, String> = Ok(String::from("it's ok"));
    let error: Result<String, String> = Err(String::from("it's not ok"));

    println!("ok: {:?}", ok);
    println!("error: {:?}", error);

    let can_unwrap_ok = ok.unwrap();
    println!("{}", can_unwrap_ok);

    // let cannot_unwrap_error = error.unwrap(); // will panic!
}

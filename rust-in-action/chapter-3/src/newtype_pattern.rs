struct Hostname(String);

#[allow(dead_code)]
fn connect(host: Hostname) {
    println!("connected to {}", host.0);
}

pub fn demo() {
    let ordinary_string = String::from("localhost");
    let _host = Hostname(ordinary_string.clone());

    // connect(ordinary_string); // compile error
}

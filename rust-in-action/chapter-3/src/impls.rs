#[derive(Debug)]
struct File {
    name: String,
    data: Vec<u8>,
}

impl File {
    fn new(name: &str) -> File {
        File {
            name: String::from(name),
            data: Vec::new(),
        }
    }

    fn new_with_data(name: &str, data: &Vec<u8>) -> File {
        let mut f = File::new(name);
        f.data = data.clone();
        f
    }

    fn read(self: &File, save_to: &mut Vec<u8>) -> usize {
        let mut tmp = self.data.clone();
        let read_length = tmp.len();
        save_to.reserve(read_length);
        save_to.append(&mut tmp);
        read_length
    }
}

pub fn demo() {
    let a_file = File::new("a_file");

    let a_file_name = &a_file.name;
    let a_file_length = a_file.data.len();

    println!("{:?}", a_file);
    println!("{} is {} bytes long", a_file_name, a_file_length);

    let another_file_data: Vec<u8> = vec![114, 117, 115, 116, 33];
    let another_file = File::new_with_data("another_file", &another_file_data);
    println!("{:?}", another_file);

    let mut buffer: Vec<u8> = vec![];
    another_file.read(&mut buffer);
    println!("{}", String::from_utf8_lossy(&buffer));
}

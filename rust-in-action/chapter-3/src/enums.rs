pub fn demo() {
    #[derive(Debug)]
    enum Answer {
        Yes,
        No,
    }

    println!("yes: {:?}", Answer::Yes);
    println!("no: {:?}", Answer::No);
}

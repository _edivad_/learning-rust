use std::fmt::{Display, Formatter};

pub fn demo() {
    struct Animal;
    struct Human;

    trait Greeter {
        fn greet(&self) -> String;
    }

    impl Greeter for Animal {
        fn greet(&self) -> String {
            String::from("Hi, I'm an animal")
        }
    }

    impl Greeter for Human {
        fn greet(&self) -> String {
            String::from("Hi, I'm an human")
        }
    }

    let an_animal = Animal;
    let an_human = Human;

    println!("{}", an_animal.greet());
    println!("{}", an_human.greet());

    struct Person {
        name: String,
    }

    //
    // implementation of standard traits for custom types
    //

    let person = Person {
        name: String::from("Davide"),
    };
    impl Display for Person {
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            write!(f, "Person({})", self.name)
        }
    }

    println!("{}", person)
}

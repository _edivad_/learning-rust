mod enums;
mod error_management;
mod impls;
mod newtype_pattern;
mod static_and_consts;
mod structs;
mod traits;

fn main() {
    structs::demo();
    newtype_pattern::demo();
    impls::demo();
    static_and_consts::demo();
    error_management::demo();
    enums::demo();
    traits::demo();
}

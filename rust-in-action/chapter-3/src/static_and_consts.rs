static mut SHARED: i32 = 0;

#[allow(unused)]
const MEANING_OF_LIFE: i32 = 1;

fn a() {
    unsafe {
        SHARED = 1;
    }
}

fn b() {
    unsafe {
        SHARED = 2;
    }
}

#[allow(unused)]
pub fn demo() {
    // const is not inferred
    const MOL: i32 = 42;
    // MOL = 24; // compile error

    // let is a const that can be set in a next step
    let c: i32;
    c = 1;

    a();
    b();
    unsafe { println!("{}", SHARED) }
}

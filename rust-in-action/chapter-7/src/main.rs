mod btreemaps;
mod endianness;
mod hashmaps;
mod hexdump;
mod paritybit;
mod serialization;

fn main() {
    serialization::demo();
    hexdump::demo();
    endianness::demo();
    paritybit::demo();
    hashmaps::demo();
    btreemaps::demo();
}

use std::io::prelude::*;

const BYTES_PER_LINE: usize = 16;
const INPUT: &'static [u8] = br#"
    fn main() {
        println!("Hello, world!");
    }"#;

pub fn demo() -> std::io::Result<()> {
    let mut buffer: Vec<u8> = vec![];
    let _ = INPUT.read_to_end(&mut buffer);

    let mut position_in_input = 0;
    for line in buffer.chunks(BYTES_PER_LINE) {
        print!("[ox{:08x}]: ", position_in_input + line.len() - 1);
        for byte in line {
            print!("{:02x} ", byte);
        }
        println!();
        position_in_input += BYTES_PER_LINE;
    }

    Ok(())
}

use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use std::fs::File;
use std::net::{AddrParseError, Ipv6Addr};

// [STEP 2] derive Debug
#[derive(Debug)]
// [STEP 1] define wrapper
enum UpstreamError {
    IO(std::io::Error),
    Parsing(AddrParseError),
}

// [STEP 3] implement Display for wrapper
impl fmt::Display for UpstreamError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            UpstreamError::IO(err) => write!(f, "IO error: {}", err),
            UpstreamError::Parsing(err) => write!(f, "Parsing error: {}", err),
        }
    }
}

// [STEP 4] implement Display for wrapper (nothing to do since Error = Debug + Display
impl Error for UpstreamError {}

// [STEP 5] optionally implement From
impl From<AddrParseError> for UpstreamError {
    fn from(error: AddrParseError) -> Self {
        UpstreamError::Parsing(error)
    }
}

fn main() -> Result<(), UpstreamError> {
    let _f = File::open("invisible.txt").map_err(UpstreamError::IO)?;

    // let _localhost = "::1".parse::<Ipv6Addr>().map_err(UpstreamError::Parsing)?;
    let _localhost = "::1".parse::<Ipv6Addr>()?;

    Ok(())
}

mod buffer_overflow;
mod complex_example;
mod dangling_pointers;
mod greet_world;
mod iterator_invalidation;
mod race_conditions;

fn main() {
    greet_world::demo();
    complex_example::demo();
    dangling_pointers::demo();
    race_conditions::demo();
    // buffer_overflow::demo(); // uncomment for crash
    iterator_invalidation::demo();
}

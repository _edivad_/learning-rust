#[derive(Debug)]
enum Cereal {
    Barley,
    Millet,
    Rice,
    Rye,
    Spelt,
    Wheat,
}

pub fn demo() {
    let mut grains: Vec<Cereal> = vec![];
    grains.push(Cereal::Rye);
    // drop(grains); // won't compile when uncommented
    println!("{:?}", grains);
}

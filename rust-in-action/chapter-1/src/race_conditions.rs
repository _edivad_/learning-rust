pub fn demo() {
    let mut data = 100;

    // won't compile when uncommented
    // use std::thread;
    // thread::spawn(|| {
    //     data = 500;
    // });
    // thread::spawn(|| {
    //     data = 1000;
    // });
    println!("{}", data);
}

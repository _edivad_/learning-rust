#[cfg(test)]
mod test {

    fn is_strong(password: String) -> bool {
        password.len() > 5
    }

    fn is_strong_generic<T: AsRef<str>>(password: T) -> bool {
        password.as_ref().len() > 5
    }

    // #[test]
    // fn wont_compile_str_is_not() {
    //     let input = "justok";
    //     let result = is_strong(input);
    //     assert_eq!(result, true);
    // }

    #[test]
    fn it_works() {
        let input = "justok";
        let result = is_strong(String::from(input));
        assert_eq!(result, true);
    }

    #[test]
    fn it_works_too() {
        let input = "justok";
        let result = is_strong_generic(input);
        assert_eq!(result, true);
    }
}

mod strings;

fn main() {
    mimicking_pointers_with_references();
    comparing_references_and_box_to_several_types();
    printing_from_strings_provided_by_external_sources();
    creating_a_raw_pointer();
    identifying_a_values_address();
    dereferencing_a_pointer();
    allocating_and_deallocating_memory_on_the_heap_via_box();
    printing_the_address_of_variables_within_a_program();
}

fn mimicking_pointers_with_references() {
    static B: [u8; 10] = [99, 97, 114, 114, 121, 116, 111, 119, 101, 108];
    static C: [u8; 11] = [116, 104, 97, 110, 107, 115, 102, 105, 115, 104, 0];

    let a = 42;
    let b = &B;
    let c = &C;

    println!("a: {}, b: {:p}, c: {:p}", a, b, c);
}

fn comparing_references_and_box_to_several_types() {
    use std::mem::size_of;

    static B: [u8; 10] = [99, 97, 114, 114, 121, 116, 111, 119, 101, 108];
    static C: [u8; 11] = [116, 104, 97, 110, 107, 115, 102, 105, 115, 104, 0];

    let a: usize = 42;
    let b: &[u8; 10] = &B;
    let c: Box<[u8]> = Box::new(C);

    println!("a (an unsigned integer):");
    println!("  location: {:p}", &a);
    println!("  size:     {:?} bytes", size_of::<usize>());
    println!("  value:    {:?}", a);
    println!();

    println!("b (a reference to B):");
    println!("  location:  {:p}", &b);
    println!("  size:      {:?} bytes", size_of::<&[u8; 10]>());
    println!("  points to: {:p}", b);
    println!();

    println!("c (a \"box\" for C):");
    println!("  location:  {:p}", &c);
    println!("  size:      {:?} bytes", size_of::<Box<[u8]>>());
    println!("  points to: {:p}", c);
    println!();

    println!("B (an array of 10 bytes):");
    println!("  location: {:p}", &B);
    println!("  size:     {:?} bytes", size_of::<[u8; 10]>());
    println!("  value:    {:?}", B);
    println!();

    println!("C (an array of 11 bytes):");
    println!("  location: {:p}", &C);
    println!("  size:     {:?} bytes", size_of::<[u8; 11]>());
    println!("  value:    {:?}", C);
}

fn printing_from_strings_provided_by_external_sources() {
    use std::borrow::Cow;
    use std::ffi::CStr;
    use std::os::raw::c_char;

    static B: [u8; 10] = [99, 97, 114, 114, 121, 116, 111, 119, 101, 108];
    static C: [u8; 11] = [116, 104, 97, 110, 107, 115, 102, 105, 115, 104, 0];

    let a = 42;
    let b: String;
    let c: Cow<str>;

    unsafe {
        let b_ptr = &B as *const u8 as *mut u8;
        // b = String::from_raw_parts(b_ptr, 10, 10); // FIXME
        let c_ptr = &C as *const u8 as *const c_char;
        c = CStr::from_ptr(c_ptr).to_string_lossy();
    }

    // println!("a: {}, b: {}, c: {}", a, b, c);
    println!("a: {}, c: {}", a, c);
}

fn creating_a_raw_pointer() {
    let a: i64 = 42;
    let a_ptr = &a as *const i64;

    println!("a: {} ({:p})", a, a_ptr);
}

fn identifying_a_values_address() {
    let a: i64 = 42;
    let a_ptr = &a as *const i64;
    let a_addr: usize = unsafe { std::mem::transmute(a_ptr) };

    println!("a: {} ({:p}...0x{:x})", a, a_ptr, a_addr + 7);
}

fn dereferencing_a_pointer() {
    let ptr = 42 as *const Vec<String>;

    unsafe {
        let new_addr = ptr.offset(4);
        println!("{:p} -> {:p}", ptr, new_addr);
    }
}

fn heap_variables() {
    let a = 40;
    let b = Box::new(60);
    // let result = a + b; // won't compile
    let result = a + *b; // <-- dereference
    println!("{} + {} = {}", a, b, result);
}

fn allocating_and_deallocating_memory_on_the_heap_via_box() {
    let a = Box::new(1);
    let b = Box::new(1);
    let c = Box::new(1);

    println!("{}, {:p}", *a, a);

    let result1 = *a + *b + *c;

    drop(a);
    // println!("{}, {:p}", *a, a); // <-- invalid now

    let d = Box::new(1);
    let result2 = *b + *c + *d;

    println!("{} {}", result1, result2);
}

fn printing_the_address_of_variables_within_a_program() {
    let local_str = "a";
    let local_int = 123;
    let boxed_str = Box::new('b');
    let boxed_int = Box::new(789);
    let fn_int = noop();

    println!("GLOBAL:    {:p}", &GLOBAL as *const i32);
    println!("local_str: {:p}", local_str as *const str);
    println!("local_int: {:p}", &local_int as *const i32);
    println!("boxed_int: {:p}", Box::into_raw(boxed_int));
    println!("boxed_str: {:p}", Box::into_raw(boxed_str));
    println!("fn_int:    {:p}", fn_int);
}

static GLOBAL: i32 = 1000;

fn noop() -> *const i32 {
    let noop_local = 12345;
    &noop_local as *const i32
}

use std::time::{Duration, Instant};

pub fn demo() {
    //
    // if, if-else
    //
    let item = 24;
    if item == 42 {
        // ...
    } else if item == 132 {
        // ...
    } else {
        // ...
    }

    //
    // for
    //

    for i in 0..10 {
        println!("{}", i);
    }

    let container = 0..10;
    for i in container {
        println!("{}", i);
    }
    // compilation error, container cannot be used twice (Java Streams?)
    // for i in container {
    //     println!("{}", i);
    // }
    // see lifetime

    // anonymous loop
    for _ in 0..3 {
        println!("Penny?");
    }

    // old for-index
    // useful when "for item in collection" is not applicable
    let collection = [1, 2, 3, 4, 5];
    for i in 0..collection.len() {
        let item = collection[i];
        println!("{}", item)
    }

    // can skip (continue)
    for i in 0..10 {
        if i % 2 == 0 {
            continue;
        }
        println!("{}", i)
    }

    //
    // while
    //

    let mut counter = 0;
    while counter < 10 {
        counter += 1;
        println!("counter: {}", counter)
    }

    // can skip (continue)
    let mut counter = 0;
    while counter < 10 {
        counter += 1;
        if counter % 2 == 0 {
            continue;
        }
        println!("counter: {}", counter)
    }

    // while using duration as condition
    let mut count = 0;
    let time_limit = Duration::new(1, 0);
    let start = Instant::now();

    while (Instant::now() - start) < time_limit {
        count += 1;
    }
    println!("{}", count);

    // infinite loop
    // while true {
    //     println!("Are we there yet?");
    // }

    //
    // loop
    //

    let mut counter = 0;
    loop {
        if counter < 3 {
            println!("Penny?");
        } else {
            break;
        }
        counter += 1;
    }

    // break can abort any loop

    let n = loop {
        break 123;
    };

    println!("break can return a value: {}", n);

    // the control is inside the block (while true)

    // break from nested loops (use labels)
    'outer: for x in 0.. {
        for y in 0.. {
            for z in 0.. {
                let sum = x + y + z;
                if sum > 1000 {
                    break 'outer;
                }
                println!("sum: {}", sum);
            }
        }
    }

    //
    // match, pattern matching
    //

    let description = match item {
        0 => "zero",                      // specific value
        10..=20 => "ten to twenty eight", // range
        40 | 80 => "forty or eighty",     // or
        _ => "something else",            // everything else (as Scala)
    };
    println!("match is: {}", description)

    // compile error, missing cases
    // match item {
    //     0 => {}
    // }
}

pub fn demo() {
    // create a vector
    let mut numbers: Vec<usize> = vec![];

    // add elements
    numbers.push(1);
    numbers.push(2);
    numbers.push(3);
    println!("{:?}", numbers);

    // remove elements
    numbers.remove(1);
    println!("{:?}", numbers);

    // check if empty
    println!("{}", numbers.is_empty());

    // iterate
    for n in numbers.iter() {
        print!("{} ", n)
    }
}

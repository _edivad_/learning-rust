use std::time::Duration;

pub fn demo() {
    //
    // function definition with lifetime
    //

    fn add_with_lifetimes<'a, 'b>(i: &'a i32, j: &'b i32) -> i32 {
        *i + *j
    }

    let a = 10;
    let b = 20;
    let res = add_with_lifetimes(&a, &b);

    println!("{}", res);

    //
    // generic function definition
    //

    // compile error, Rust don't know how to do addition for T
    // fn add_generic<T>(i: T, j: T) -> T {
    //     i + j
    // }

    // need of traits, Scala's Type Classes
    fn add_generic<T: std::ops::Add<Output = T>>(i: T, j: T) -> T {
        i + j
    }

    println!("ints: {}", add_generic(40, 2));
    println!("floats: {}", add_generic(40.0, 2.0));
    println!(
        "durations: {:?}",
        add_generic(Duration::new(32, 0), Duration::new(10, 0))
    );
}

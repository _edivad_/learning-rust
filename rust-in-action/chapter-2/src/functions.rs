pub fn demo() {
    //
    // function definition (local to main)
    //

    fn add(i: i32, j: i32) -> i32 {
        i + j
    }

    println!("{}", add(40, 2));
}

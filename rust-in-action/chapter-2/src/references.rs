pub fn demo() {
    // as C

    let a = 42;
    let r = &a; // reference operation
    let b = a + *r; // dereference operation

    // reference and dereference operators are unary operators

    println!("a + a = {}", b);

    let needle = 0o204;
    let haystack = [1, 1, 2, 5, 15, 52, 203, 877, 4140, 21147];

    // avoid the copy (see lifetime)
    for item in &haystack {
        if *item == needle {
            println!("{}", item);
        }
    }
}

pub fn demo() {
    //
    // expressions
    //

    // if is an expression
    let n = 123456;
    let description = if is_even(n) { "even" } else { "odd" };
    println!("{} is {}", n, description);

    // match is an expression
    let n = 654321;
    let description = match is_even(n) {
        true => "even",
        false => "odd",
    };
    println!("{} is {}", n, description);

    // also break is an expression
    let n = loop {
        break 123;
    };
    println!("{}", n);

    //
    // not expressions (statements) (Unit in Scala)
    //
    let _a_common_statement = println!("hello");

    let mut _n = 0;
    let _a_bind = _n = 1;

    // type declarations
}

fn is_even(n: i32) -> bool {
    return n % 2 == 0;
}

pub fn demo() {
    let a = 10; // type in inferred
    let b: i32 = 20; // type can be specified
    let c = 30i32; // type specified as suffix
    let d = 40_i32; // type specified as suffix more readable

    let e = add(add(a, b), add(c, d));

    println!("( a + b ) + ( c + d ) = {}", e)
}

// type defined for parameters and return value
fn add(i: i32, j: i32) -> i32 {
    // return not needed
    i + j
}

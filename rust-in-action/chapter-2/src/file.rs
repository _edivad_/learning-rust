use std::fmt::Debug;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

pub fn demo() {
    //
    // read text file, raw way
    //

    let f = File::open("../../../README.md").unwrap();
    let mut reader = BufReader::new(f);

    let mut line = String::new();

    loop {
        let len = reader.read_line(&mut line).unwrap();
        if len == 0 {
            break;
        }

        println!("{} ({} bytes long)", line, len);

        line.truncate(0);
    }

    //
    // read text file, better way
    //

    let f = File::open("../../../README.md").unwrap();
    let reader = BufReader::new(f);

    for line_ in reader.lines() {
        let line = line_.unwrap();
        println!("{} ({} bytes long)", line, line.len());
    }

    // read missing file
    let r = File::open("MISSING.md");
    match r {
        Ok(f) => eprintln!("unexpected"),
        Err(s) => println!("error opening file: {}", s.to_string()),
    }
}

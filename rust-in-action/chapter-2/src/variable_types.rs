pub fn demo() {
    numbers();
}

fn numbers() {
    /*
       signed
    */
    title("Signed integer");

    let positive_byte: i8 = 64;
    let negative_byte: i8 = -128;
    // let overflow_byte: i8 = 128; // compilation error, does not fit
    println!("i8: {}, {}", positive_byte, negative_byte);

    let positive_two_bytes: i16 = 128;
    let negative_two_bytes: i16 = -256;
    // let overflow_two_bytes: i16 = -32769;
    println!("i16: {}, {}", positive_two_bytes, negative_two_bytes);

    let number = 42;
    println!("i32 (default): {}", number);

    let long_number: i64 = -1_000_000_000_000;
    println!("i64: {}", long_number);

    /*
       unsigned
    */
    title("Unsigned integer");

    let byte: u8 = 255;
    // let overflow_byte: u8 = -1; // compilation error, cannot be negative
    println!("u8: {}", byte);

    let two_bytes: u16 = 60_000;
    println!("u16: {}", two_bytes);

    let number = 42;
    println!("u32 (default): {}", number);

    let long_number: u64 = 1_000_000_000_000;
    println!("u64: {}", long_number);

    /*
       non-base 10
    */
    let three_base2 = 0b11;
    let thirty_base8 = 0o36;
    let three_hundred_bse16 = 0x12C;

    println!(
        "base 10: {} {} {}",
        three_base2, thirty_base8, three_hundred_bse16
    );
    println!(
        "base 2:  {:b} {:b} {:b}",
        three_base2, thirty_base8, three_hundred_bse16
    );
    println!(
        "base 8:  {:o} {:o} {:o}",
        three_base2, thirty_base8, three_hundred_bse16
    );
    println!(
        "base 16: {:x} {:x} {:x}",
        three_base2, thirty_base8, three_hundred_bse16
    );

    /*
       floating point
    */
    title("Floating point");

    let a_simple_float: f32 = 24.0;
    println!("f32: {}", a_simple_float);

    let a_float = 42.0;
    println!("f64 (default): {}", a_float);

    /*
       native
    */
    title("Native");

    let native_signed_integer: isize = 42;
    let native_unsigned_integer: usize = 24;
    println!("isize: {}", native_signed_integer);
    println!("usize: {}", native_unsigned_integer);

    /*
       comparing numbers
    */
    title("Comparing numbers");

    println!("1 < 2 -> {}", 1 < 2);
    println!("2 > 1 -> {}", 2 > 1);
    println!("1 == 1 -> {}", 1 == 1);
    println!("1 != 1 -> {}", 1 != 1);
    println!("...");

    /*
       conversion
    */
    title("Conversion");

    // let r = 10_i32 < 100_i64; // compile error, cannot compare different types

    let _r = 10 < (100_i64 as i32); // possible after cast

    // let r = 1_000 as i8; // compile error, cast does not fit

    let a = 100;
    let b: u16 = 1_000;
    let b_ = b.try_into().unwrap(); // conversion guided by context (try change type of a)
    println!("a < b -> {}", a < b_);

    /*
       floating-point hazards
    */
    title("Floating-point hazards");
    println!("0.1 + 0.2 = 0.3 ? -> {}", 0.1 + 0.2 == 0.3);
    let x: f64 = (0.1 + 0.2) - 0.3;
    println!(
        "(0.1 + 0.2) - 0.3 = +/- epsilon ? -> {}",
        (x).abs() <= f64::EPSILON
    );
}

fn title(title: &str) {
    println!("\n***");
    println!("{}", title);
}

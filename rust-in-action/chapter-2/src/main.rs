mod advanced_functions;
mod arrays;
mod expressions;
mod file;
mod flow_control;
mod functions;
mod references;
mod variable_types;
mod variables_definition;
mod vectors;

fn main() {
    variables_definition::demo();
    variable_types::demo();
    flow_control::demo();
    expressions::demo();
    functions::demo();
    references::demo();
    advanced_functions::demo();
    arrays::demo();
    vectors::demo();
    file::demo();
}

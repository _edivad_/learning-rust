mod cpu;
mod fixed_point_number_formats;
mod floating_point_numbers;

fn main() {
    format_print_and_unsafe();
    // integer_overflow(); // will panic when run
    endianness();
    floating_point_numbers::demo();
    fixed_point_number_formats::demo();
    random::demo();
    cpu::demo();
}

fn format_print_and_unsafe() {
    let a: f32 = 42.42;
    let frankentype: u32 = unsafe { std::mem::transmute(a) };

    println!("{}", frankentype);
    println!("{:032b}", frankentype);

    let b: f32 = unsafe { std::mem::transmute(frankentype) };
    println!("{}", b);
    assert_eq!(a, b);
}

fn integer_overflow() {
    let mut i: u16 = 0;
    print!("{}..", i);

    loop {
        i += 1000;
        print!("{}..", i);
        if i % 10000 == 0 {
            print! {"\n"}
        }
    }
}

fn endianness() {
    let big_endian: [u8; 4] = [0xAA, 0xBB, 0xCC, 0xDD];
    let little_endian: [u8; 4] = [0xDD, 0xCC, 0xBB, 0xAA];

    let a: i32 = unsafe { std::mem::transmute(big_endian) };
    let b: i32 = unsafe { std::mem::transmute(little_endian) };

    println!("{} vs {}", a, b);
}

mod random {
    fn mock_rand_with_division(n: u8) -> f32 {
        (n as f32) / 255.0
    }

    fn mock_rand_without_division(n: u8) -> f32 {
        let base: u32 = 0b0_01111110_00000000000000000000000;
        let large_n = (n as u32) << 15;
        let f32_bits = base | large_n;
        let m = f32::from_bits(f32_bits);
        2.0 * (m - 0.5)
    }

    pub fn demo() {
        println!(
            "max of input range: {:08b} -> {:?}",
            0xff,
            mock_rand_without_division(0xff)
        );
        println!(
            "mid of input range: {:08b} -> {:?}",
            0x7f,
            mock_rand_without_division(0x7f)
        );
        println!(
            "min of input range: {:08b} -> {:?}",
            0x00,
            mock_rand_without_division(0x00)
        );
    }
}

#[cfg(test)]
mod test {
    use std::mem::transmute;

    #[test]
    fn endianness_using_mask_and_shift() {
        let n: u16 = 0x607B;
        let lsb = (n & 0xff) as u8;
        let msb = (n >> 8 & 0xff) as u8;
        assert_eq!(lsb, 0x7B);
        assert_eq!(msb, 0x60);
    }

    #[test]
    fn endianness_using_transmute() {
        let n: u32 = 0x6000007B;
        let a: [u8; 4] = unsafe { transmute(n) };
        assert_eq!(a[0], 0x7B);
        assert_eq!(a[1], 0x00);
        assert_eq!(a[2], 0x00);
        assert_eq!(a[3], 0x60);
    }
}

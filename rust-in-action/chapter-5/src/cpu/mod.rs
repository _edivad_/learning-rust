mod cpu1;
mod cpu2;
mod cpu3;

pub fn demo() {
    cpu1::demo();
    // cpu2::demo(); moved to tests
    // cpu3::demo(); moved to tests
}

# Learning Rust

## Playground

- [Here](playground/README.md)

## Books

- [Rust in Action](rust-in-action/README.md)

## References

- [Rust Programming Language](https://www.rust-lang.org/)
- [Leard Rust](https://www.rust-lang.org/learn/)
- [The Rust Programming Language](https://doc.rust-lang.org/book/)
- [Rust by Example](https://doc.rust-lang.org/rust-by-example/)
- [rustlings](https://github.com/rust-lang/rustlings)

